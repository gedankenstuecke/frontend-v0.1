import React, { Component, Fragment } from "react";
import Api from 'Api';
import { injectIntl } from "react-intl";
import PeopleList from "Components/People/PeopleList";
import PeopleHeader from "../Components/People/PeopleHeader";
import Loading from "../Components/Tools/Loading";
import Filter from "Components/Tools/Filter";

class PeoplePage extends Component {

  constructor(props){
    super(props);
    this.state = {
      listPeople: [],
			loading: true,
			loadingBtn: false,
			loadBtnCount: 1,
			hideLoadBtn: false,
    };
  }

  myCallback = dataFromChild => {
    this.setState({ listDataFromChild: dataFromChild }); // send data to filter component (child)
	};
	
	loadUsers(currentPage) {
		var itemsPerQuery = 100 // number of users per query calls (load more btn click)
		if(currentPage > 1) this.setState({loadingBtn: true});
    Api.get(`/api/users?items=${itemsPerQuery}&page=${currentPage}`).then(res=>{ // temp fix to display 500 items, before setting up pagination @TOFIX
			// var listPeople = [];
			var listPeople = this.state.listPeople;
			var users = res.data
			users.filter(user => user.confirmed_at !== null ) // don't show users that haven't confirmed their account
			.sort((a, b) => b.follower_count - a.follower_count) // sort by popularity, to get most popular users of each query call
			.map((user) => { // map
        return listPeople.push(user);
			});
			this.setState({listPeople, loading: false, loadingBtn: false, loadBtnCount: currentPage += 1});
			
    }).catch(error=>{
			this.state.hideLoadBtn = true // hide btn when it's last call
      this.setState({ loading: false, loadingBtn: false });
    });
	}

  componentDidMount(){
		this.setState({loading: true});
		this.loadUsers(this.state.loadBtnCount)
  }

  render(){
		const { listPeople, loading, loadingBtn, hideLoadBtn } = this.state;
		const { intl } = this.props;
		// console.log(listPeople);
		var btnHiddenClass = hideLoadBtn ? "d-none" : ""
		var showFilterClass = !hideLoadBtn ? "d-none" : ""
    return (
      <div className="container-fluid peoplePage">
        {/*<PeopleMap />*/}
        <PeopleHeader />
				<div className={`row peopleSearch ${showFilterClass}`}>
					<div className="col-12">
						<Filter list={listPeople} callback={this.myCallback} type="people"/>
					</div>
				</div>
        <Loading active={loading} height="300px">
          <div className="justify-content-center">
						<Fragment>
            	<PeopleList searchBar={false} listPeople={listPeople} colMax={1} />
							<button type="button" className={`btn btn-primary loadBtn ${btnHiddenClass}`}
                    onClick={() => this.loadUsers(this.state.loadBtnCount)}>
								{loadingBtn &&
		              <Fragment>
		             		<span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true"></span>&nbsp;
		              </Fragment>
		            }
								{intl.formatMessage({ id: "general.load" })}
							</button>
						</Fragment>
          </div>
        </Loading>
				<div className={`row peopleSearch ${showFilterClass}`}>
					<div className="col-12">
						<Filter list={listPeople} callback={this.myCallback} type="people"/>
					</div>
				</div>
      </div>
    );
  }

}
export default injectIntl(PeoplePage);