import React, { Component } from "react";
import "./NewJogler.scss";
import { FormattedMessage } from "react-intl";

export default class NewJogler extends Component {

  render(){
    return (
      <div className="container-fluid newJogler">
        <div className="alert alert-success" role="alert">
          <h4 className="alert-heading">
            <FormattedMessage id="newJogler.title" defaultMessage="Congratulations !" />
          </h4>
          <p>
            <FormattedMessage id="newJogler.message" defaultMessage="Your account is now activated on JoGL !" />
          </p>
        </div>
        <div className="inviteSignIn">
          <p className="text-center">
            <FormattedMessage id="newJogler.inviteConnect" defaultMessage="Please sign in to complete your profile" />
            <br/>
            <br/>
            <button type="button" className="btn btn-primary" data-toggle="modal" data-target="#signInModal">
              <FormattedMessage id="header.signIn" defaultMessage="Sign in" />
            </button>
          </p>
        </div>
      </div>
    );
  }

}
