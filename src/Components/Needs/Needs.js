import React, { Component } from "react";
import Api from 'Api';
import NeedCreate from "Components/Needs/Need/NeedCreate";
import NeedDisplay from "Components/Needs/Need/NeedDisplay";
import "./Needs.scss";

export default class Needs extends Component {

  constructor(props){
    super(props);
    this.state = {
      needs: [],
    }
  }

  static get defaultProps() {
    return {
      displayCreate : false,
      itemId : undefined,
      itemType : undefined,
    }
  }

  getNeedsApi() {
    const {itemId, itemType } = this.props;
    if(!itemId || !itemType){
      // console.log("Missing props to call API");
    } else {
      Api.get("/api/" + itemType + "/" + itemId + "/needs")
      .then(res=>{
        this.setState({needs: res.data});
      }).catch(error=>{
        // console.log(error);
      });
    }
  }

  componentDidMount(){
    this.getNeedsApi();
  }

  refresh() {
    this.getNeedsApi();
  }

  displayNeeds(needs) {
    if(needs.length !== 0) {
      return(
        needs.reverse().map((need, index) => (
          <div className="col-12 col-lg-6" key={index}>
          	<NeedDisplay need={need} refresh={this.refresh.bind(this)} />
					</div>
        ))
      )
		}
  }

  render(){
    const { displayCreate, itemId } = this.props;
    const { needs } = this.state;
    return (
      <div className="needs">
        {displayCreate &&
          <NeedCreate project_id={itemId} 
                      refresh={this.refresh.bind(this)} />
        }
				<div className="row">
					{this.displayNeeds(needs)}
				</div>
      </div>
    );
  }
}
