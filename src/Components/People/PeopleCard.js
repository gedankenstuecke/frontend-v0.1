import React, { Component } from "react";
import BtnFollow from "../Tools/BtnFollow";
import { UserContext } from 'UserProvider';
import { Link } from "react-router-dom";
import ModalContactForm from "../Tools/ModalContactForm";
import defaultImg from "assets/img/default/default-user.png";
import "./PeopleCard.scss";

export default class PeopleCard extends Component {
  constructor(props){
    super(props);
    this.state = {
      user: this.props.user,
    }
  }

  static get defaultProps() {
    return {
      user : undefined,
    }
  }

  render(){
    let userContext = this.context;
    const link = "#";
    if(this.props.user !== undefined){
			var user = this.props.user;
			var {logo_url} = this.props.user;
			// console.log(user.logo_url);
			// var logo_url;
			// if(user.logo_url === "" || user.logo_url === undefined || user.logo_url === null || user.logo_url.includes('https://robohash.org')){
			if(user.logo_url === "" || user.logo_url === undefined || user.logo_url === null){
	      logo_url = defaultImg;
			}
      return (
        <div className="peopleCard" key={user.id}>
          <Link to={"/user/" + (user.id)}>
            <div className="peopleCard--profileImg"
                 style={{"backgroundImage" : "url(" + logo_url + ")"}} />
          </Link>
          <div className="peopleCard--userInfo">
            <Link to={"/user/" + (user.id)}>
              <h6 className="userName">
                {(user.first_name !== null ? user.first_name : "Prénom") + " " + (user.last_name !== null ? user.last_name : "Nom")}
              </h6>
            </Link>
            <div className="userBio">
							{/* if user has short bio, display it, else display as empty */}
							{user.short_bio ? user.short_bio : user.bio ? user.bio : "_ _" }
            </div>
          </div>
					{userContext.user &&
	          <div className="peopleCard--buttons">
	            <div className="container-fluid">
	              <div className="row">
	                {userContext.user.id !== user.id &&
	                	<BtnFollow followState={user.has_followed} itemType="users" itemId={user.id} classBtn="btn btn-sm btn-primary btn-action btn-card" />
	                }
									{userContext.user.id !== user.id && user.can_contact !== false && <ModalContactForm itemId={user.id} itemType="user"/>}
	              </div>
	            </div>
	          </div>
					}
        </div>
      );
    } else {
      return null;
    }
  }
}
PeopleCard.contextType = UserContext;
