import React, { Component } from "react";
import { injectIntl } from "react-intl";
import PeopleSearch from "./PeopleSearch";
import ListComponent from "../Tools/ListComponent";
import "./PeopleList.scss";

class PeopleList extends Component {

  constructor(props){
    super(props);
    // console.log("PeopleList - props : ", this.props);
    this.state = {
      listPeople: this.props.listPeople,
      searchBar: this.props.searchBar
    };
  }

  static get defaultProps() {
    return {
      listPeople: [],
			colMax: 2,
			filter: false,
			itemType: ""
    }
  }

  render(){
		const { intl, filter, listPeople, itemType, searchBar, colMax } = this.props;
		if(filter) {
			var memberName = itemType === "challenges" ? "entity.card.participants" : "member.role.members" 
	    return (
	      <div className="peopleList">
	        {/* {searchBar && <PeopleSearch /> } */}
					<h4>{intl.formatMessage({id:'member.role.creators'})}</h4>
	        <ListComponent itemType="people" filter="creators" data={listPeople} colMax={colMax} />
					<hr/>
					<h4>{intl.formatMessage({id:'member.role.admins'})}</h4>
					<ListComponent itemType="people" filter="admins" data={listPeople} colMax={colMax} />
					<hr/>
					<h4>{intl.formatMessage({id:memberName})}</h4>
					<ListComponent itemType="people" filter="members" data={listPeople} colMax={colMax} />
	    	</div>
			);
		}
		else {
			return (
				<div className="peopleList">
			    {searchBar && <PeopleSearch /> }
					<ListComponent itemType="people" data={listPeople} colMax={colMax} />
				</div>
			);
		}
  }
}
export default injectIntl(PeopleList)