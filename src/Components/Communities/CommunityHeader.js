import React, { Component, Fragment } from "react";
import { FormattedMessage } from "react-intl";
import { Link } from "react-router-dom";
import defaultImg from "assets/img/default/default-group.jpg";
import BtnFollow from "../Tools/BtnFollow";
import BtnJoin from "../Tools/BtnJoin";
import InfoInterestsComponent from "Components/Tools/Info/InfoInterestsComponent";
import InfoSkillsComponent from "Components/Tools/Info/InfoSkillsComponent";
import BtnClap from "../Tools/BtnClap";
import { renderStatsModal, renderOwnerNames } from 'Components/Tools/utils/utils.js'

export default class CommunityHeader extends Component {

  editBtn(){
    if (this.props.community.is_admin) {
      return(
        <Link to={"/community/" + this.props.community.id + "/edit"}>
          <i className="fa fa-edit"/>
          <FormattedMessage id="entity.form.btnAdmin" defaultMessage="Edit" />
        </Link>
      )
    }
  }

  render(){
    var {banner_url, follower_count, has_followed, id, is_member, is_owner, members_count, short_description, short_title, title, skills, status, interests, has_clapped, claps_count, users, creator} = this.props.community;
    if(follower_count === undefined) follower_count = 0;
    if(members_count === undefined) members_count = 0;
    if(banner_url === "" || banner_url === undefined || banner_url === null){
      banner_url = defaultImg;
    }
    const bannerStyle = {
      backgroundImage: "url(" + banner_url + ")"
    };
    return(
      <div className="row communityHeader">
        <div className="col-12 title">
          <h1>{title}</h1>
          {this.editBtn()}
        </div>
        <div className="col-lg-7 col-md-12 communityHeader--banner">
          <div style={bannerStyle}></div>
        </div>
        <div className="col-lg-5 col-md-12 communityHeader--info">
					{/* <p className="infos">#{short_title}<span>({status})</span></p> */}
					<p className="infos">#{short_title}</p>
          {/* <p className="info"><strong>Description:</strong><br/>{short_description}</p> */}
					<p className="info">{short_description}</p>
					{users !== undefined && users.length > 0 && renderOwnerNames(users, creator)}
          <InfoInterestsComponent place="entity_header" title="" content={interests} />
          <InfoSkillsComponent place="entity_header" title="" content={skills} />
          <div className="communityStats">
						<span className="text" data-toggle="modal" data-target="#followersModal">
							<strong>{follower_count}</strong>&nbsp;
							<FormattedMessage id="user.profile.followers" defaultMessage="Followers" />{follower_count > 1 ? "s" : ""}
						</span>
            <span className="text" data-toggle="modal" data-target="#entityMembersModal">
							<strong>{members_count}</strong>&nbsp;
							<FormattedMessage id="entity.info.members" defaultMessage="Member" />{members_count > 1 ? "s" : ""}
						</span>
          </div>
          <div className="zoneBtnActions">
            <BtnFollow  followState={has_followed}
                        itemType="communities" itemId={id}
                        textFollow={<FormattedMessage id="community.info.btnFollow" defaultMessage="Follow community" />}
                        textUnfollow={<FormattedMessage id="community.info.btnUnfollow" defaultMessage="Unfollow community" />} />
            {!is_owner &&
              <BtnJoin  joinState={is_member}
                        itemType="communities" itemId={id} />
						}
						<BtnClap itemType="communities" itemId={id} clapState={has_clapped} clapCount={claps_count}/>
          </div>
        </div>
				{follower_count ? renderStatsModal("followersModal", "communities", this.props.community) : "" /* trigers popup only if there are followers */}
				{renderStatsModal("entityMembersModal", "communities", this.props.community)}
      </div>
    );
  }
}
