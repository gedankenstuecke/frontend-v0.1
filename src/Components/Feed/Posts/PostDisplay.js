import defaultImgUser from "assets/img/default/default-user.png";
import defaultImgProject from "assets/img/default/default-project.jpg";
// import defaultImgGroup from "assets/img/default/default-group.jpg";
import CommentDisplay from "Components/Feed/Comments/CommentDisplay";
import BtnClap from "Components/Tools/BtnClap";
import Api from 'Api';
import DocumentsList from "Components/Tools/Documents/DocumentsList.js";
import React, { Component, Fragment } from "react";
import { FormattedMessage, injectIntl } from "react-intl";
import { Link } from "react-router-dom";
import PostDelete from "./PostDelete";
import PeopleList from "Components/People/PeopleList";
import "./PostDisplay.scss";
import PostUpdate from "./PostUpdate";
import { linkify } from 'Components/Tools/utils/utils.js'
import $ from 'jquery';
// import Slider from "react-slick";

class PostDisplay extends Component {

	constructor(props){
		super(props);
		this.state = {
			showComments: true,
			edit: false,
      clappedUsers: [],
      gotClappers: false
		};
	}

  static get defaultProps() {
		return {
			post: undefined,
			feedId: undefined,
			isAdmin: false,
			user: "",
			isHomeFeed: false,
    }
	}

	changeDisplayComments(){
    this.setState({showComments : !this.state.showComments});
	}

	isEdit() {
		this.setState({"edit": !this.state.edit});
	}

	getuserInfo(userId, index, clappersNb) {
		Api.get("/api/users/" + userId)
	    .then(res=>{
				var arr = this.state.clappedUsers
				arr.push(res.data); // push new data to array
				if(index+1 == clappersNb) { // when getting last user
					this.forceUpdate(); // force render update
				}	
	    }).catch(error=>{
			});
  }
  
  showClapModal() {
		var post = this.props.post
		var clappersNb = post.clappers.length
		if(post.clappers.length != 0) {
			if(!this.state.gotClappers) { // do this only once
				post.clappers.map((clapper, index) => {
					// for each clappers, get his info. Passing user id, map index, and how many clappers (to force update when getting last user)
					this.getuserInfo(clapper.user_id, index, clappersNb)
				})
			}
			this.setState({gotClappers: true});
			$(`#clappersModal${post.id}`).modal("show") // show modal
		}
	}
		
	renderClappersModal() {
		var post = this.props.post
    return (
			<div className="modal fade clappersModal" id={`clappersModal${post.id}`} tabIndex="-1" role="dialog" aria-labelledby="clappersModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="clappersModalLabel">
								<i className="fa fa-sign-language" />
              </h5>
              <button type="button" id="closeModal" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
							{/* Show modal content only if there are clappers*/}
              {this.state.clappedUsers.length != 0 && <PeopleList listPeople={this.state.clappedUsers} searchBar={false} itemType="people" colMax={1} />}
            </div>
          </div>
        </div>
      </div>
		);
  }

  render(){
		// var settings = {
		// 	dots: true,
		// 	arrows: true,
		// 	infinite: true,
		// 	fade: true,
		// 	lazyload: true,
    //   speed: 500,
		// 	swipeToSlide: true,
		// 	draggable: true,
    //   slidesToShow: 1,
		// 	slidesToScroll: 1,
    //   afterChange: current => this.setState({ activeSlide: current })
		// };
		// const { data } = this.state

		const {intl, user, refresh, post, feedId, isAdmin, isHomeFeed} = this.props;
		const postId = post.id;
		const creatorId = post.creator.id
    const objImg = post.creator.logo_url ? post.creator.logo_url : defaultImgUser;
		// var objImg;
		var postCreatorName = post.creator.first_name + " " + post.creator.last_name
		var postFromName = post.from.object_type === "need" // if post is a need
		? intl.formatMessage({id:'post.need'}) + post.from.object_name
		: post.from.object_name
		var postCreatorLink = "/user/" + creatorId
		var postFromLink = post.from.object_type === "need" // if post is a need
		? "/project/"+post.from.object_need_proj_id+"#needs"  // link to the /project not /need because it doesn't exist
		: "/"+post.from.object_type+"/"+post.from.object_id // else it's the default link
		
		var defaultImg = post.from.object_type === "project" ? defaultImgProject : defaultImgUser;
		// var defaultImg =(post.from.object_type === "project" || post.from.object_type === "need") ? defaultImgProject : post.from.object_type === "community" ? defaultImgGroup : defaultImgUser;

		// if(isHomeFeed) { // if feed is homepage feed
		// 	if(post.from.object_type !== "user") { // check if it comes from any object aside from user
		// 		objImg = post.from.object_image ? post.from.object_image : defaultImg;
		// 	}
		// 	else objImg = post.creator.logo_url ? post.creator.logo_url : defaultImgUser;
		// } 
		// else { // else it's from a user's feed
		// 	objImg = post.creator.logo_url ? post.creator.logo_url : defaultImg;
		// } 
		var moment = require('moment');
		const lang = localStorage.getItem("language")
		if(lang === "fr") require('moment/locale/fr.js');
		var commentsCount = post.comments.length
		// console.log(moment().format('X'));
		// console.log(moment());
		// console.log(moment(post.created_at));
		// console.log(moment(post.created_at).format("X"));
		var postDate = (moment().format('X') - moment(post.created_at).format('X') > 600000) ?
			moment(post.created_at).calendar()
		:
			moment(post.created_at).startOf('hour').fromNow()

		// const objImgStyle = {backgroundImage: "url(" + objImg + ")"}
		const userImgStyle = {backgroundImage: "url(" + objImg + ")"}

    if(post !== undefined) {
			// console.log("POST : ", post);
			var post_images = post.documents.filter(document => (document.content_type === "image/jpeg" || document.content_type === "image/png"));
			const contentWithLinks = linkify(post.content)
	    return(
				<div className="post">
					<div className="topContent">
						<div className="topBar">
							<div className="left">
								<div className="userImgContainer">
									<Link to={"/user/" + creatorId}>
										<div className="userImg" style={userImgStyle}></div>
									</Link>
									{/* <Link to={isHomeFeed ? postFromLink : postCreatorLink}>
										<div className="userImg" style={objImgStyle}></div>
									</Link> */}
								</div>
								<div className="topInfo">
									<Link to={"/user/" + creatorId}>
										{post.creator.first_name + " " + post.creator.last_name}
									</Link>
									{/* <Link to={isHomeFeed ? postFromLink : postCreatorLink}>
										{isHomeFeed ? postFromName : postCreatorName}
									</Link> */}
									<div className="date">{postDate}</div>
									<div className="from">
										<FormattedMessage id="post.from" defaultMessage="From" />
										<Link to={postFromLink}>
											{postFromName}
										</Link>
										{/* <Link to={isHomeFeed ? postCreatorLink : postFromLink}>
											{isHomeFeed ? postCreatorName : postFromName}
										</Link> */}
									</div>
								</div>
							</div>
							<div className="post-manage right d-flex flex-row">
							{(user.id === creatorId || this.props.isAdmin) &&
								<div className="btn-group dropright">
								  <button type="button" className="btn btn-secondary dropdown-toggle" data-display="static" data-flip="false" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								    •••
								  </button>
								  <div className="dropdown-menu dropdown-menu-right">
										{/* {user.id === creatorId && */}
											<Fragment>
												{!this.state.edit ?
													<div onClick={this.isEdit.bind(this)}><i className="fa fa-edit postUpdate"/> <FormattedMessage id="feed.object.update" defaultMessage="Update" /></div>
												:
													<div onClick={this.isEdit.bind(this)}>
														<FormattedMessage id="feed.object.cancel" defaultMessage="Cancel" />
													</div>
												}
												<PostDelete postId={postId} type="post" refresh={refresh}/>
											</Fragment>
										{/* } */}
										{/* <div onClick={console.log("report")}><i className="fa fa-flag postFlag"/> <FormattedMessage id="feed.object.report" defaultMessage="Report" /></div> */}
								  </div>
								</div>
							}
							</div>
						</div>
						{this.state.edit ?
							<PostUpdate postId={postId} content={post.content} isEdit={this.isEdit.bind(this)} refresh={refresh} user={user} />
						:
							<div className="text extra" dangerouslySetInnerHTML={{ __html: contentWithLinks }} />
						}
						{post_images.length > 0 && <img className="postImage" alt="preview" src={post_images[0].url}/>}

						{/* <Slider ref={c => (this.slider = c)} {...settings}>
							{post_images.map((image, index) => (
								<img className="postImage" src={image.url} key={index}/>
			      	))}
		     		</Slider> */}

						<DocumentsList documents={post.documents} cardtype="feed" refresh={refresh} postObj={post} user={user} isEditing={this.state.edit}/>
						<div className="postStats">
							{/* on clap icon click, show modal */}
							<span onClick={() => this.showClapModal()}>
								<i className="fa fa-sign-language" /> {post.claps_count}
							</span>
							<span onClick={this.changeDisplayComments.bind(this)}>
								<i className="fa fa-comment" /> {commentsCount} <FormattedMessage id="post.comments" defaultMessage="Comment" />{commentsCount > 1 ? "s" : ""}
							</span>
						</div>

					</div>

					<div className="actionBar">
						<div className="meta">
							<BtnClap itemType="posts" itemId={post.id} type="text" clapState={post.has_clapped} refresh={refresh}/>
							<button className="btn-postcard btn"
											onClick={this.changeDisplayComments.bind(this)}>
								<i className="fa fa-comments" /> <FormattedMessage id="post.comment" defaultMessage="Comment" />
							</button>
						</div>
						{this.state.showComments && <CommentDisplay comments={post.comments} postId={postId} feedId={feedId} refresh={refresh} user={user} isAdmin={isAdmin}/>}
					</div>
					{this.renderClappersModal()}
				</div>
			);
		}
		else return null;
  }
}
export default injectIntl(PostDisplay)