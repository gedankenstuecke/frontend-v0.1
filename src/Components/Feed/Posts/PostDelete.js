import React, { Component } from "react";
import Api from 'Api';
import { FormattedMessage } from "react-intl";

export default class PostDelete extends Component {

  static get defaultProps() {
    return {
			postId: undefined,
			commentId: undefined,
			type: undefined
    }
  }

  deletePost(){
		const postId = this.props.postId
		const commentId = this.props.commentId
    if(postId !== undefined){
			var apiUrl = this.props.type === "post" ? `/api/posts/${postId}` : `/api/posts/${postId}/comment/${commentId}`
      Api.delete(apiUrl)
      .then(res=>{
        // console.log(res);
        this.props.refresh()
      })
      .catch(error=>{
        // console.log(error);
      })
    } else {
      // console.log("undefined postId");
    }
  }

  render(){
    return (
			<div onClick={this.deletePost.bind(this)}><i className="fa fa-trash postDelete"/> <FormattedMessage id="feed.object.delete" defaultMessage="Delete" /></div>
    );
  }
}
