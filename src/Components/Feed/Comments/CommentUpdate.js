import React, { Component } from "react";
import Api from 'Api';
import FormComment from "./FormComment"
import { findMentions, noHTMLMentions, transformMentions } from "Components/Feed/Mentions.js"

export default class CommentUpdate extends Component {

  constructor(props){
    super(props);
    this.state = {
      content: noHTMLMentions(this.props.content),
      mentions: [],
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  static get defaultProps() {
    return {
      postId: undefined,
      content: "",
      commentId: 1,
      user: ""
    }
  }

  componentWillReceiveProps(nextProps){
    this.setState({postId: nextProps.postId});
  }

  handleChange(content){
    this.setState({content});
  }

  handleSubmit(event) {
    var mentions = findMentions(this.state.content);
    var contentNoMentions = transformMentions(this.state.content);
    const postId = this.props.postId
    const commentId = this.props.commentId
    var updateJson = {
      "comment": {
        "content": contentNoMentions
      }
    };
    if(mentions){
      updateJson["comment"]["mentions"] = mentions;
    }
    // console.log("updateJson", updateJson);
    Api.patch(`/api/posts/${postId}/comment/${commentId}`, updateJson)
    .then(res => {
      // console.log(res);
      this.props.isEdit();
      this.props.refresh();
    })
    .catch(error => {
      // console.log(error);
    });
  }

  // findMentions(content){
  //   if(content){
  //     var regexMentions = /\(\(.*?:.*?:.*?\)\)/g
  //     const match = content.match(regexMentions);
  //     if(match) {
  //       var mentions = match.map((mention) => {
  //         mention = mention.substring(2, mention.length - 2);
  //         var type = mention.split(":")[0] === "@" ? "user" : "project";
  //         const id = mention.split(":")[1];
  //         const content = mention.split(":")[2];
  //         return {"obj_type": type, "obj_id": id, "obj_match": type + content}
  //       });
  //       return mentions;
  //     } else {
  //       return [];
  //     }
  //   } else {
  //     return [];
  //   }
  // }

  // transformMentions(content){
  //   if(content){
  //     var regexMentions = /\(\(.*?:.*?:.*?\)\)/g
  //     const match = content.match(regexMentions);
  //     if(match) {
  //       match.forEach((mention) => {
  //         var mention_obj = mention.substring(2, mention.length - 2)
  //         var obj_type = mention_obj.split(":")[0] === "@" ? "user" : "project";
  //         content = content.replace(mention,
  //           "<a href=\"/" + obj_type + "/" + mention_obj.split(":")[1] + "\">" +
  //           mention_obj.split(":")[0] + mention_obj.split(":")[2] + "</a>");
  //       });
  //     }
  //   }
  //   return content;
  // }

  render(){
    return (
        <FormComment
          action="update"
          content={this.state.content}
          handleChange={this.handleChange}
          handleSubmit={this.handleSubmit}
          user={this.props.user}
        />
    );
  }
}
