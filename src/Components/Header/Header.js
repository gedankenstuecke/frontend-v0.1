import React, { Component } from "react";
import { Link } from "react-router-dom";
import { FormattedMessage, injectIntl }  from "react-intl";
import Logo from "assets/img/logo_img.png";
import UserMenu from "../User/UserMenu";
import "./Header.scss";
import $ from 'jquery';
import { connectStateResults } from "react-instantsearch/connectors";
import {
  InstantSearch,
  Hits,
  SearchBox,
	Index,
	Configure,
} from 'react-instantsearch-dom';
import algoliasearch from 'algoliasearch/lite';

class Header extends Component {

  constructor(props){
    super(props);
    this.state = {
			listPrograms: [],
			lang: "en"
    };
  }

	static get defaultProps() {
		return {
			userConnected: false,
    }
	}

  changeLanguage(newLanguage) {
    localStorage.setItem("language", newLanguage)
    window.location.reload();
	}

  componentDidMount(){
		// collapse menu + reset searchbar when clicking on a menu link
		var navMain = $("#navbarNav");
		navMain.on("click", "a", null, function () {
		  navMain.collapse('hide');
		});
		// when clicking anywhere on page..
		$(document).on('click', function (e){
	    var menu_opened = $('#navbarNav').hasClass('show');
	    if(!$(e.target).closest('#navbarNav').length &&
	       !$(e.target).is('#navbarNav') &&
	       menu_opened === true){
					$('#navbarNav').collapse('toggle'); // collapse mobile menu if open..
				}
				// + reset searchbar
				if(document.querySelector(".search--mobile input").value != "" || document.querySelector(".search--desktop input").value != "") {
					document.querySelector(".search--mobile .ais-SearchBox-reset").click()
					document.querySelector(".search--desktop .ais-SearchBox-reset").click()
				}
		});

		// manage lang dropdown
		var userLang = navigator.language || navigator.userLanguage;
		if(localStorage.getItem("language")) { // if user is signed-in or has selected a lang
			if(localStorage.getItem("language") === "fr") {
				$(".dropdown-item.fr").addClass("checked")
				this.setState({lang: "fr"});
			}
			else {
				$(".dropdown-item.en").addClass("checked")
				this.setState({lang: "en"});
			}
		}
		else { // if user is not signed-in or it's his first time
		if(userLang === "fr-FR" || userLang === "fr") {
				$(".dropdown-item.fr").addClass("checked")
				this.setState({lang: "fr"});
			}
			else {
				$(".dropdown-item.en").addClass("checked")
				this.setState({lang: "en"});
			}
		}
  }

  render(){
		const { intl } = this.props;
		var connectClass = this.props.userConnected ? "connected" : "notConnected"

		// ------  START OF ALGOLIA INSTANT SEARCH BAR  ---------- //
		var appId = process.env.REACT_APP_ALGOLIA_APP_ID;
		var token = process.env.REACT_APP_ALGOLIA_TOKEN;
		const searchClient = algoliasearch(appId, token);
		// function that transform each query result/hit into a link with the object title/name.
		function Hit(props) {
			if(props.hit.title) // if it has a title, it's a project
		  return (
				<a href={"/project/" + props.hit.objectID}>
					{props.hit.title}
				</a>
			);
			else return ( // else it's a user
				<Link to={"/user/" + props.hit.objectID}>
					{props.hit.first_name + " " + props.hit.last_name}
				</Link>
			);
		}
		// show results if search has query, else show "no result" message 
		const Content = connectStateResults(
			({ searchState, searchResults }) =>
				searchResults && searchResults.nbHits !== 0 ? (
					<Hits hitComponent={Hit} />
				) : (
					<div>
						<FormattedMessage id="header.search.noresult" defaultMessage="No results has been found for " />
						"{searchState.query}"
					</div>
				)
		);
		// show results of query only when we start typing
		const Results = connectStateResults(
			({ searchState }) =>	
				searchState && searchState.query ? (
					<div className="searchResults">
						<Index indexName="Project">
							<h5><FormattedMessage id="header.linkProjects" defaultMessage="Projects" /></h5>
							<Configure hitsPerPage="5" />
							<Content />
						</Index>
						<Index indexName="User">
							<h5><FormattedMessage id="members.title" defaultMessage="Members" /></h5>
							<Configure hitsPerPage="5" />
							<Content />
						</Index>
					</div>
				) :
				null
		);
		// ------  END OF ALGOLIA INSTANT SEARCH BAR  ---------- //

		return (
			<header className={`navContainer ${connectClass}`}>
				<nav className="nav container-fluid navbar navbar-expand-lg navbar-light d-flex justify-content-left">
					<Link to="/" className="navbar-brand">
						<img src={Logo} className="logo" alt="JoGL icon" />
					</Link>
					<div className="menu">
						<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
							<span className="navbar-toggler-icon"></span>
						</button>
						<div className="collapse navbar-collapse" id="navbarNav">
							<ul className="navbar-nav">
								{/* start - Algolia Instant Search */}
								<div className="search--desktop">
									<InstantSearch indexName="Project" searchClient={searchClient}>
										<SearchBox 
											translations={{
												placeholder: intl.formatMessage({id:'header.search'})
											}}
										/>
										<Results />
								  </InstantSearch>
								</div>
								{/* end - Algolia Instant Search */}
								<li className="nav-item">
									<Link to="/needs" className="nav-link">
										<FormattedMessage id="entity.tab.needs" defaultMessage="Needs" />
									</Link>
								</li>
								<li className="nav-item">
									<Link to="/people" className="nav-link">
										<FormattedMessage id="header.linkPeople" defaultMessage="People" />
									</Link>
								</li>
								<li className="nav-item">
									<Link to="/projects" className="nav-link">
										<FormattedMessage id="header.linkProjects" defaultMessage="Projects" />
									</Link>
								</li>
								<li className="nav-item">
									<Link to="/communities" className="nav-link">
										<FormattedMessage id="header.linkCommunities" defaultMessage="Communities" />
									</Link>
								</li>
	              <li className="nav-item">
	               <Link to="/challenges" className="nav-link">
	                 <FormattedMessage id="header.linkChallenges" defaultMessage="Challenges" />
	               </Link>
	              </li>
								<li className="nav-item dropdown">
					        <span className="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	                 <FormattedMessage id="programs" defaultMessage="Programs" />
					        </span>
					        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
										<Link to="/program/coimmune" className="dropdown-item">Co-Immune</Link>
					        </div>
					      </li>
								{!this.props.userConnected ? // if user is connected, show usermenu on header, not in dropdown
									<UserMenu />
								: null}
							</ul>
							<div className="dropdown-divider"></div>
							<ul className="navbar-nav ml-auto">
							{this.props.userConnected ? // show create action only when user is connected
								<li className="nav-item dropdown create">
									<span className="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<FormattedMessage id="entity.form.btnCreate" />
									</span>
									{/* <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown"> */}
									<div className="dropdown-menu" aria-labelledby="navbarDropdown">
										<Link to="/project/create" className="dropdown-item">
											<FormattedMessage id="header.createProject" />
										</Link>
										<div className="dropdown-divider"></div>
										<Link to="/community/create" className="dropdown-item">
											<FormattedMessage id="header.createGroup" />
										</Link>
									</div>
								</li>
								: ""
							}
								<li className="nav-item dropdown langSelect">
									<span className="nav-link dropdown-toggle" id="langDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i className="fas fa-globe"></i> {this.state.lang}
									</span>
                  <div className="dropdown-menu" aria-labelledby="langDropdown">
                  	<a className="dropdown-item en" onClick={() => this.changeLanguage("en")}>English <i className="fas fa-check"></i></a>
                  	<a className="dropdown-item fr" onClick={() => this.changeLanguage("fr")}>Français <i className="fas fa-check"></i></a>
                	</div>
              	</li>
							</ul>
						</div>
					</div>
					{/* start - Algolia Instant Search */}
					<div className="search--mobile">
						<InstantSearch indexName="Project" searchClient={searchClient}>
							<SearchBox 
								translations={{
									placeholder: intl.formatMessage({id:'header.search'})
								}}
							/>
							<Results />
					  </InstantSearch>
					</div>
					{/* end - Algolia Instant Search */}
					<UserMenu />
				</nav>
			</header>
		);
  }
}
export default injectIntl(Header)