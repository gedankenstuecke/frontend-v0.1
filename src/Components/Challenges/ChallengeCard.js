import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import defaultChallengeImg from "assets/img/default/default-challenge.jpg";
import defaultLogoImg from "assets/img/default/default-user.png";
import { renderOwnerNames } from 'Components/Tools/utils/utils.js'
import BtnClap from "../Tools/BtnClap";
import "../Main/Cards.scss";

export default class ChallengeCard extends Component {
  constructor(props){
    super(props);
    this.state = {
      challenge: this.props.challenge,
    }
  }

  static get defaultProps() {
    return {
      challenge : undefined,
    }
  }

  renderChallengeTeam(users) {
    return (
			<div className="bubbleTeam">
				<span>
        <FormattedMessage id="entity.card.participants" defaultMessage="Participants" />
        ({users.length}): </span>
				<span className="imgList">
	        {users.map((user, index) => {
						var logoUrl = !user.logo_url ? defaultLogoImg : user.logo_url
						const logoStyle = {
							backgroundImage: "url(" + logoUrl + ")",
						};
	          if(index < 6) {
	            return (
	              <Link to={"/user/" + user.id} key={index}>
								 <div className="peopleImg" style={logoStyle}></div>
								</Link>
	            );
	          } else if(index === 6) {
	            return (
								<Link to={"/challenge/" + (this.props.challenge.id)} key={index}>
									<div className="moreMembers">+{users.length - 6}</div>
								</Link>
	            );
	          }
	          return "";
					})}
				</span>
			</div>
    );
  }

  render(){
    if(this.props.challenge !== undefined){
			// console.log(this.props.challenge);
			var { id, title, title_fr, banner_url, short_description, short_description_fr, users, has_clapped, claps_count, projects_count, program, creator } = this.props.challenge;
			var challenge_program = program[0] ? program[0] : program // program[0] if we are in the program challenge tab (temp fix)
			var userLang = navigator.language || navigator.userLanguage; 
			const lang = (localStorage.getItem("language") && localStorage.getItem("language") === "fr") || (!localStorage.getItem("language") && (userLang === "fr-FR" || userLang === "fr")) ? "fr" : "en"
      if(banner_url === undefined || banner_url === null || banner_url === ""){
        banner_url = defaultChallengeImg;
			}
			const challengeImgStyle = { backgroundImage: 'url(' + banner_url + ')', }
      if(this.props.cardFormat !== "compact") {
        return (
          <div className="card cardChallenge" key={id}>
            <Link to={"/challenge/" + (id)}>
              <div style={challengeImgStyle} className="challengeImg"/>
            </Link>
            <BtnClap itemType="challenges" itemId={id} clapState={has_clapped} clapCount={claps_count}/>
            <div className="card-content">
  						<Link to={"/challenge/" + (id)}>
  	            <h5 className="card-title">{lang === "fr" && title_fr ? title_fr : title}</h5>
  						</Link>
							<p className="card-shortname">
								{challenge_program && challenge_program.id !== -1 && // if program id is !== -1 (meaning challenge is not attached to a program), display program name and link
									<Fragment>
										<FormattedMessage id="entity.info.program.title" defaultMessage="program" />
										<span><Link to={"/program/" + challenge_program.short_title}>
										{lang === "fr" && challenge_program.title_fr ? challenge_program.title_fr : challenge_program.title}
										</Link></span>
									</Fragment>
								}
							</p>
              <p className="card-desc">{lang === "fr" && short_description_fr ? short_description_fr : short_description}</p>
              {/* <p className="card-desc">{projects_count}</p> */}
							<p className="card-desc projects_count">{projects_count ? projects_count : 0} {projects_count > 1 ? <FormattedMessage id="challenge.info.submittedProjects" defaultMessage="Projects submitted" /> : <FormattedMessage id="challenge.info.submittedProjectSingle" defaultMessage="Project submitted" />}</p>
							{users !== undefined && users.length > 0 && renderOwnerNames(users, creator)}
							{/* <InfoSkillsComponent place="entity_card" limit="3" content={skills} /> */}
  					</div>
      			<div className="cardfooter">
              <hr/>
              {users !== undefined && users.length > 0 && this.renderChallengeTeam(users)}
      			</div>
          </div>
        );
      }
      else {
        return (
          <div className="card cardChallenge cardChallengeSmall" key={id}>
          <Link to={"/challenge/" + (id)}>
            <div style={challengeImgStyle} className="challengeImg"/>
          </Link>
            <BtnClap itemType="challenges" itemId={id} clapState={has_clapped}  clapCount={claps_count}/>
            <div className="card-content">
  						<Link to={"/challenge/" + (id)}>
  	            <h5 className="card-title">{title}</h5>
  						</Link>
              <p className="card-desc">{short_description}</p>
            </div>
          </div>
        );
      }
    } else {
      return null;
    }
  }
}
