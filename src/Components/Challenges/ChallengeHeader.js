import React, { Component, Fragment } from "react";
import { FormattedMessage, FormattedDate, injectIntl } from "react-intl";
import { Link } from "react-router-dom";
import defaultImg from "assets/img/default/default-challenge.jpg";
import BtnAdd from "../Tools/BtnAdd";
import BtnFollow from "../Tools/BtnFollow";
import BtnJoin from "../Tools/BtnJoin";
import ListProjectsAttached from "../Tools/ListProjectsAttached";
import PeopleList from "../People/PeopleList";
import $ from 'jquery';
import "./ChallengeHeader.scss";

class ChallengeHeader extends Component {

  static get defaultProps() {
    return {
      lang: "en",
    }
  }

  editBtn(){
    if (this.props.challenge.is_admin) {
      return(
        <Link to={"/challenge/" + this.props.challenge.id + "/edit"}>
          <i className="fa fa-edit"/>
          <FormattedMessage id="entity.form.btnAdmin" defaultMessage="Edit" />
        </Link>
      )
    }
	}

  renderStatsModal(type, challenge) {
		var networkType = type === "entityProjectsModal" ? "projects" : "participants"
    return (
			<div className="modal fade statsModal" id={type} tabIndex="-1" role="dialog" aria-labelledby="statsModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h3 className="modal-title" id="statsModalLabel">
								<FormattedMessage id={`entity.tab.${networkType}`} defaultMessage="Followers" />
              </h3>
              <button type="button" id="closeDeleteModal" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
							{type === "entityProjectsModal" &&
                <ListProjectsAttached itemId={challenge.id} itemType="challenges" />
							}{type === "participantsModal" &&
								<PeopleList filter={true} listPeople={challenge.users} searchBar={false} itemType="challenges"/>
							}
            </div>
          </div>
        </div>
      </div>
    );
  }
	
	exploreProjects() {
		document.querySelector(".challengeStats span:nth-child(1)").click() // click on the projects counter link, to open popup
	}
	
	addProj() {
		document.querySelector('.zoneBtnActions .btn[data-target="#modalAddProject"]').click();
	}
  
  getDaysLeft(end_date){
		const now = new Date();
    const end = new Date(end_date);
		var daysLeft = (end - now) / 1000 / 60 / 60 / 24;
    if(daysLeft <= 0){
      daysLeft = 0;
		}
    return Math.ceil(daysLeft);
  }

  render(){
    const {
      banner_url,
      end_date,
      final_date,
      // follower_count,
      has_followed,
      id,
      is_member,
      is_owner,
      launch_date,
      members_count,
      projects_count,
			status,
			// short_title,
			title,
			title_fr,
			program,
			short_description,
			short_description_fr
		} = this.props.challenge;


		$("body").click(function(e) { // on click, check 
			if($('.statsModal').hasClass('show')) { // if modal is open
				if ((e.target.class == "statsModal" || $(e.target).parents(".statsModal").length) // then, if we're clicking inside the modal
				&& e.target.class !== "close" ) { // and if we're not cliking on the close button
					$('.statsModal').modal('hide'); // close popup
				}
			}
		});
		
		const { intl } = this.props;
		var challengeStatus = this.getDaysLeft(final_date) === 0 ? "completed" : status
		var bannerUrl = banner_url ? banner_url : defaultImg

    return(
			<div className="challengeHeader">
				<img src={bannerUrl} width="100%"/>
				<div className="challengeHeader--title">
					<h1 className="text-center">Challenge <span>
					{"{ "+(this.props.lang === "fr" && title_fr ? title_fr : title)+" }"}
					</span></h1>
					{this.editBtn()}
				</div>
				{/* <p>#{short_title}</p> */}
				<div className="row challengeHeader--info">
					<div className="col-12 col-md-3 order-md-1 programAndDates">
						<p className="info">
							{program.id !== -1 ? // if program id is !== -1 (meaning challenge is not attached to a program), display program name and link
								<Fragment>
									<strong><FormattedMessage id="entity.info.program.title" defaultMessage="Program" /></strong>
									<span><Link to={"/program/" + program.short_title}>{program.title}</Link></span>
								</Fragment>
								: // else display no program text
								<FormattedMessage id="challenge.noProgram" defaultMessage="No program" />
							}
						</p>
						<div className="statusContainer">
							<span className="status">{intl.formatMessage({id:`entity.info.status.${challengeStatus}`})}</span>&nbsp;
							{this.getDaysLeft(final_date) > 0 &&
								<Fragment>
									{"("+this.getDaysLeft(final_date)+" "}
									<FormattedMessage id="program.due.days" defaultMessage="days" />{this.getDaysLeft(final_date) > 1 ? "s" : ""}
									<FormattedMessage id="program.due.left" defaultMessage="left" />)
								</Fragment>
							}
							<br/>
							<a className="btn btn-primary" data-toggle="collapse" href="#collapseDates" role="button" aria-expanded="false" aria-controls="collapseDates">
								{intl.formatMessage({id:"program.dates"})}
						  </a>
						</div>
						<div className="collapse" id="collapseDates">
						  <div className="card card-body">
								<strong><FormattedMessage id="entity.info.launch_date" defaultMessage="Launch" />: </strong>
								{launch_date ? <FormattedDate value={launch_date} year="numeric" month="long" day="2-digit" /> : <FormattedMessage id="general.noDate" defaultMessage="No date" />}
								<br/>
								<strong><FormattedMessage id="entity.info.final_date" defaultMessage="Final Submissions" />: </strong>
								{final_date ? <FormattedDate value={final_date} year="numeric" month="long" day="2-digit" /> : <FormattedMessage id="general.noDate" defaultMessage="No date" />}
								<br/>
								<strong><FormattedMessage id="entity.info.end_date" defaultMessage="Winner Announced" />: </strong>
								{end_date ? <FormattedDate value={end_date} year="numeric" month="long" day="2-digit" /> : <FormattedMessage id="general.noDate" defaultMessage="No date" />}
						  </div>
						</div>
					</div>
					<div className="col-12 col-md-6 order-first order-md-2 short_desc text-center">
						<p>{this.props.lang === "fr" &&short_description_fr ?short_description_fr :short_description}</p>
						{/* <p>{intl.formatMessage({ id: 'challenge.temp.coimmune.shortDesc' })}</p> */}
					</div>
					<div className="zoneBtnActions col-12 col-md-3 order-md-3">
						<div className="challengeBtns justify-content-end">
							<BtnFollow followState={has_followed}
								itemType="challenges" itemId={id}
								textFollow={<FormattedMessage id="challenge.info.btnFollow" defaultMessage="Follow challenge" />}
								textUnfollow={<FormattedMessage id="challenge.info.btnUnfollow" defaultMessage="Unfollow challenge" />} />
							{/* Contribute dropdown */}
							<div className="dropdown">
							  <button className="btn btn-primary btn-md dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									{intl.formatMessage({id:"challenge.info.btnParticipate"})}
							  </button>
							  <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
									<div className="dropdown-item">
										<button className="btn btn-md btn-outline-primary" type="button" onClick={this.exploreProjects}>
											{intl.formatMessage({id:"challenge.info.btnExplore"})}
									  </button>
									</div>
			            {!is_owner && // show join button only if we are not owner of challenge
									<div className="dropdown-item">
			              <BtnJoin  joinState={is_member}
			                        itemType="challenges"
			                        itemId={id}
			                        textJoin={<FormattedMessage id="challenge.info.btnJoin" defaultMessage="Join challenge" />}
			                        textUnjoin={<FormattedMessage id="challenge.info.btnUnjoin" defaultMessage="Leave challenge" />} />
									</div>
									}
									<div className="dropdown-item">
										<button className="btn btn-md btn-outline-primary" type="button" onClick={this.addProj}>
											{intl.formatMessage({id:"attach.project.title"})}
									  </button>
									</div>
								</div>
							</div>
							{/* Contribute dropdown */}
							<div className="hiddenAddBtn"> {/* // add project btn, had to hide it and the btn inside dropdown clicks it, else the modal appears in the dropdown box */}
								<BtnAdd type="project" itemType="challenges" showText={false} itemId={id} /> 
							</div>
						</div>
						<div className="challengeStats text-right">
							<span className="text" data-toggle="modal" data-target="#entityProjectsModal">
								<strong>{projects_count ? projects_count : 0}</strong>&nbsp;
								{projects_count > 1 ? <FormattedMessage id="challenge.info.submittedProjects" defaultMessage="Projects submitted" /> : <FormattedMessage id="challenge.info.submittedProjectSingle" defaultMessage="Project submitted" />}
							</span><br/>
	            <span className="text" data-toggle="modal" data-target="#participantsModal">
								<strong>{members_count ? members_count : 0}</strong>&nbsp;
								<FormattedMessage id="entity.info.participants" defaultMessage="Participants" />{members_count > 1 ? "s" : ""}
							</span>
		        </div>
					</div>
				</div>
				{projects_count ? this.renderStatsModal("entityProjectsModal", this.props.challenge) : "" /* trigers popup only if there are projects */}
				{this.renderStatsModal("participantsModal", this.props.challenge)}
			</div>
    );
  }
}
export default injectIntl(ChallengeHeader)