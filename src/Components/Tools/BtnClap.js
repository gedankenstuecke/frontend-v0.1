import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import Api from 'Api';
import "./BtnClap.scss";

export default class BtnClap extends Component {
  constructor(props){
    super(props);
    this.state = {
      clapState: this.props.clapState,
      clapCount: this.props.clapCount,
      sending: false,
    }
  }

  static get defaultProps() {
    return {
      clapState: false,
      clapCount: 0,
      itemId: undefined,
      itemType: undefined,
      type: "btn",
    }
  }

  componentWillReceiveProps(nextProps){
    const {clapNb, clapState, itemId, clapCount} = nextProps;
    this.setState({clapNb, clapState, itemId, clapCount});
  }

  changeStateClap(action){
    const { itemId, itemType } = this.props;
    const { clapCount } = this.state;
    if(!itemId || !itemType){
    } else {
			this.setState({sending: true});
			if(action == "clap") {
				console.log("clap");
	      Api.put("/api/" + itemType + "/" + itemId + "/clap")
	      .then(res=>{
	        this.setState({clapCount: clapCount++, clapState: true, sending: false});
	      })
				.catch(error=>{ this.setState({sending: false}); })
			}
			else {
				console.log("unclap");
				Api.delete("/api/" + itemType + "/" + itemId + "/clap")
				.then(res=>{
					this.setState({clapCount: clapCount--, clapState: false, sending: false});
				})
				.catch(error=>{ this.setState({sending: false}); })
			}
			if(this.props.refresh !== undefined){
				this.props.refresh();
			}
    }
  }

  render(){
    const { clapState, sending, clapCount } = this.state;
    const { type } = this.props;

    switch(type){
      case "text":
        return(
          <div onClick={() => this.changeStateClap((clapState ? "unclap" : "clap"))}
                className={
                  clapState ? "btn-postcard btn text-primary" : "btn-postcard btn text-dark"
                }
                disabled={sending}>
            {sending ?
              <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true"> &nbsp;</span>
            :
              <i className="fa fa-sign-language" />
            }
            <FormattedMessage id="post.clap" defaultMessage="Clap" />
          </div>
        );
      default:
        return(
          <button onClick={() => this.changeStateClap((clapState ? "unclap" : "clap"))}
                    className={
                      clapState ? "btn-clap btn btn-sm btn-primary text-light" : "btn-clap btn btn-sm btn-secondary text-dark"
                    }
                    id="btnClap"
                    disabled={sending}>
            <i className="fa fa-sign-language"/>
            <span>{clapCount}</span>
            {sending &&
              <span className="spinInsideClap"><span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true"></span></span>
            }
          </button>
        );
    }
  }
}
