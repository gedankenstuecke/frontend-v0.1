import React, { Fragment } from "react";
import { FormattedMessage } from "react-intl";
import { Link } from "react-router-dom";
import ListFollowers from "Components/Tools/ListFollowers";
import PeopleList from "Components/People/PeopleList";
import defaultLogoImg from "assets/img/default/default-user.png";
import $ from 'jquery';

export function linkify(content) { // detect links in a text and englobe them with a <a> tag
	var urlRegex =/(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)(?:\([-A-Z0-9+&@#/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#/%=~_|$?!:,.]*\)|[A-Z0-9+&@#/%=~_|$])/igm;
	// var urlRegex =/(?:(?:https?|ftp):\/\/|\b(?:[a-z\d]+\.))(?:(?:[^\s()<>]+|\((?:[^\s()<>]+|(?:\([^\s()<>]+\)))?\))+(?:\((?:[^\s()<>]+|(?:\(?:[^\s()<>]+\)))?\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))?/ig;
	// var urlRegex =/[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi;
  return content.replace(urlRegex, function(url) {
		var addHttp = url.substr(0,3) === "www" ? "http://" : ""
    return '<a href="' + addHttp+url + '" target="_blank">' + url + '</a>';
  });
}

export function linkifyQuill(text) { // detect links oustide of <a> tag and linkify them (only for quill content)
  var exp = /((href|src)=["']|)(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
  return text.replace(exp, function() {
    return  arguments[1] ? 
            arguments[0] : 
            "<a href=\"" + arguments[3] + "\" target='_blank'>" + arguments[3] + "</a>"
  });
}
export function stickyTabNav(isEdit) {
	// Get the tab navigation
	var navtabs = document.querySelector(".nav-tabs");
	// Get the offset position of the tab navigation
	if(navtabs) { // check if nav
		var sticky = navtabs.offsetTop - 80; // equals to the offset from top of navtab - 80px (header height)
		// When the user scrolls the page, execute stickyScrollTop
		stickyScrollTop() // launch function once
		window.onscroll = function() {stickyScrollTop()}; // redo function on scroll
		// Add the sticky class to the tab navigation when you reach its scroll position. Remove "sticky" when you leave the scroll position
		function stickyScrollTop() {
			if (window.pageYOffset >= sticky) { // when we arrive at the level of the tab navigation div
				navtabs.classList.add("sticky") // add class sticky
			} else {
				navtabs.classList.remove("sticky");
			}
		}
		$('.nav-item.nav-link').on('click', function(){
			if(!isEdit) window.location.hash = $(this)[0].hash
			window.scrollTo({top: sticky, behavior: 'smooth'}); // stickyValue is dynamically 0 or sticky depending if the tab navigation is sticky
	 });
	}
}

export function scrollToActiveTab() {
	// takes the hash in the url, if it match the href of a tab, then force click on the tab to access content
	if(window.location.hash && document.body.contains(document.querySelector(`a[href="${window.location.hash}"]`))) {
		$(`a[href="${window.location.hash}"]`)[0].click();
		var element = document.querySelector(`.tab-pane.active`); // get active tab content
		const y = element.getBoundingClientRect().top + window.pageYOffset - 200; // calculate it's top value and remove 140 of offset
		window.scrollTo({top: y, behavior: 'smooth'}); // scroll to tab
	}
}

export function renderStatsModal(type, objType, obj){
	var networkType = type === "followersModal" ? "followers" : "members"
	$("body").click(function(e) { // on click, check 
		if($('.statsModal').hasClass('show')) { // if modal is open
			if ((e.target.class == "statsModal" || $(e.target).parents(".statsModal").length) // then, if we're clicking inside the modal
			&& e.target.class !== "close" ) { // and if we're not cliking on the close button
				$('.statsModal').modal('hide'); // close popup
			}
		}
	});
	return (
		<div className="modal fade statsModal" id={type} tabIndex="-1" role="dialog" aria-labelledby="statsModalLabel" aria-hidden="true">
			<div className="modal-dialog" role="document">
				<div className="modal-content">
					<div className="modal-header">
						<h3 className="modal-title" id="statsModalLabel">
							<FormattedMessage id={`entity.tab.${networkType}`} defaultMessage="Followers" />
						</h3>
						<button type="button" id="closeDeleteModal" className="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div className="modal-body">
						{type === "followersModal" &&
							<ListFollowers itemId={obj.id} itemType={objType} />
						}{type === "entityMembersModal" &&
							<PeopleList filter={true} listPeople={obj.users} searchBar={false} />
						}
					</div>
				</div>
			</div>
		</div>
	);
}

export function renderOwnerNames(users, projectCreator) {
	var noOwner = true;
	users.map((user) => { if(user.owner) noOwner = false; }) // map through users, and set noOwner to false if there are no owners
	return (
		<p className="card-by"><FormattedMessage id="entity.card.by" defaultMessage="by " />
			{users.filter(user => user.owner).map((user, index, users) => { // filter to get only owners, and then map
				const rowLen = users.length
				if(index < 6) {
					return (
						<Fragment key={index}>
							 <Link to={"/user/" + (user.id)}>
								{user.first_name + " " + user.last_name}
							 </Link>
							{/* add comma, except for last item*/}
							{rowLen !== index + 1 && <span>, </span>}
						</Fragment>
					);
				} else if(index === 6) {
					return ("...");
				}
				return "";
			})}
			{noOwner && // if project don't have any owner, display creator as the owner
				<Link to={"/user/" + (projectCreator.id)}>{projectCreator.first_name + " " + projectCreator.last_name}</Link>
			}
		</p>
	);
}

export function renderTeam(users, objType, objId) {
	var filteredUsers = users.filter(user => (user.admin || user.owner || user.member)) // hide pending members
	return (
		<div className="bubbleTeam">
			<span><FormattedMessage id="entity.card.members" defaultMessage="Project team" /> ({filteredUsers.length}): </span>
			<span className="imgList">
				{filteredUsers.map((user, index) => { // map
					var logoUrl = !user.logo_url ? defaultLogoImg : user.logo_url
					const logoStyle = {
						backgroundImage: "url(" + logoUrl + ")",
					};
					if(index < 6) {
						return (
							<Link to={"/user/" + user.id} key={index}>
							 <div className="peopleImg" style={logoStyle}></div>
							</Link>
						);
					} else if(index === 6) {
						return (
							<Link to={"/" + objType + "/" + objId} key={index}>
								<div className="moreMembers">+{users.length - 6}</div>
							</Link>
						);
					}
					return "";
				})}
			</span>
		</div>
	);
}

export function formatNumber(value) {
  return Number(value).toLocaleString();
}

export function infiniteLoader(type) { // force click on load more button when it appears in the page 
	var counter = 0; // set counter to 0
	var loadMoreBtn = $('.btn.loadBtn'),
				hT = loadMoreBtn.offset().top, // element distance from top of page
				hH = loadMoreBtn.outerHeight(), // height of element
				wH = $(window).height(); // window height
	$(window).scroll(function() {
		var wS = $(window).scrollTop(); // current y scroll position in the page (in px)
		// if type is not feed, no condition. If it's a feed, check if we are in the news/feed tab
		if(type != "feed" || (type=="feed" && document.body.contains(document.querySelector('#news.active .btn.loadBtn')))) { // check if button exists
			if (wS > (hT+hH-wH) && counter == 0){ // execute only if element appears in the page
				loadMoreBtn.click() // force clicking on load button
				counter++ // incremente counter to click only once
			}
		}
	});
}