import React from "react";

const Alert = ({message, type}) => (
  <div className={"w-100 alert alert-" + type} role="alert">
    {message}
  </div>
);

export default Alert;