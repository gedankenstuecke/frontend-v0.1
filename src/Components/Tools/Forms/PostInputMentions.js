import React, { Component } from "react";
import { FormattedMessage, injectIntl }  from "react-intl";
import { MentionsInput, Mention } from 'react-mentions'
import algoliasearch from 'algoliasearch';


const defaultStyle = {
  control: {
    backgroundColor: '#fff',

    fontSize: 12,
    fontWeight: 'normal',
  },

  highlighter: {
    overflow: 'hidden',
  },

  input: {
    margin: 0,
  },

  '&singleLine': {
    control: {
      display: 'inline-block',

      width: 130,
    },

    highlighter: {
      padding: 1,
      border: '2px inset transparent',
    },

    input: {
      padding: 1,

      border: '2px inset',
    },
  },

  '&multiLine': {
    control: {
      border: '1px solid silver',
    },

    highlighter: {
      padding: 9,
    },

    input: {
      padding: 9,
      minHeight: 30,
      outline: 0,
      border: 0,
    },
  },

  suggestions: {
    list: {
      backgroundColor: 'white',
      border: '1px solid rgba(0,0,0,0.15)',
      fontSize: 10,
    },

    item: {
      padding: '5px 15px',
      borderBottom: '1px solid rgba(0,0,0,0.15)',

      '&focused': {
        backgroundColor: '#cee4e5',
      },
    },
  },
};

class PostInputMentions extends Component {

  constructor(props){
    super(props);
		this.handleChange = this.handleChange.bind(this);
		this.placeholder = [["post"], ["What's on your mind?"]]
  }

  static get defaultProps() {
    return {
      onChange: (value) => console.log("onChange doesn't exist to update " + value),
      style: defaultStyle,
      content: "",
    }
  }

  handleChange(event){
    this.props.onChange(event.target.value);
  }

  searchAlgoliaUsers(query, callback) {
    var appId = process.env.REACT_APP_ALGOLIA_APP_ID;
    var token = process.env.REACT_APP_ALGOLIA_TOKEN;
    var client = algoliasearch(appId, token);
    var index = client.initIndex('User');
    index.search({
      query: query,
      // attributesToRetrieve: ['nickname'],
      hitsPerPage: 10,
    })
    .then(content => {
			// const resultFormat = content.hits.map(obj => ({ id: obj.objectID, display: obj.nickname+" ("+obj.first_name+ " "+obj.last_name+")" }));
			const resultFormat = content.hits.map(obj => ({ id: obj.objectID, display: obj.nickname }));
      return resultFormat
    })
    .then(callback)
    .catch((err) => {
      // console.log(err);
    });
  }

  searchAlgoliaProjects(query, callback) {
    var appId = process.env.REACT_APP_ALGOLIA_APP_ID;
    var token = process.env.REACT_APP_ALGOLIA_TOKEN;
    var client = algoliasearch(appId, token);
		var index = client.initIndex('Project');
    index.search({
      query: query,
      // attributesToRetrieve: ['title'],
      hitsPerPage: 10,
    })
    .then(content => {
      // console.log("content : ", content);
			const resultFormat = content.hits.map(obj => ({ id: obj.objectID, display: obj.short_title }));
			// const resultFormat = content.hits.map(obj => ({ id: obj.objectID, display: obj.title+" ("+obj.short_title+")" }));
      return resultFormat;
    })
    .then(callback)
    .catch((err) => {
      // console.log(err);
    });
  }

  render(){
		const inputId = this.props.placeholder[0]+".placeholder"
		var { intl } = this.props;
    return (
      <div className="postInputMentions">
        <MentionsInput  value={this.props.content}
                        onChange={this.handleChange}
                        className="suggestionsContainer"
                        style={this.props.style}
                        displayTransform={(id, display, type) => {
                          if(type === "#"){
                            return `#${display}`;
                          } else {
                            return `@${display}`;
													}
													// return `@${display}`;
                        }}
                        markup="((__type__:__id__:__display__))"
                        placeholder={intl.formatMessage({ id: inputId })}
                        >
          <Mention
            type="@"
            // type="@@"
            trigger="@"
            data={this.searchAlgoliaUsers}
          />
          <Mention
            type="#"
            trigger="#"
            // type="#@"
            // trigger="@"
            data={this.searchAlgoliaProjects}
          />
        </MentionsInput>
      </div>
    );
  }
}
export default injectIntl(PostInputMentions)